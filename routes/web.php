<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\OxygenRequestController;



// Login Routes
Route::get('/login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('/login', [LoginController::class, 'login']);

Route::middleware(['guest'])->group(function () {
    Route::get('/login', [LoginController::class, 'showLoginForm'])->name('login');
    Route::post('/login', [LoginController::class, 'login']);
    
    Route::get('/register', [UserController::class, 'showRegistrationForm'])->name('register');
    Route::post('/register', [UserController::class, 'register']);
    Route::get('/', [DashboardController::class, 'home'])->name('home');

});

Route::get('/logout', [LoginController::class, 'logout'])->name('logout');

// Dashboard Route
Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('/filter', [DashboardController::class, 'index'])->name('filter');
    
    Route::post('/edit', [OxygenRequestController::class, 'editStatus'])->name('editStatus');

});

Route::post('/oxygen-requests', [OxygenRequestController::class, 'store'])->name('oxygen-requests.store');
Route::get('/oxygen-requests', [OxygenRequestController::class, 'create'])->name('oxygen-requests.create');
Route::get('/getQty', [OxygenRequestController::class, 'getQty'])->name('getQty');

