<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameColumnInOxygenQuantitiesTable extends Migration
{
    public function up()
    {
        Schema::table('oxygen_quantities', function (Blueprint $table) {
            $table->renameColumn('5_ltr', 'ltr_5');
            $table->renameColumn('10_ltr', 'ltr_10');
            $table->renameColumn('15_ltr', 'ltr_15');

        });
    }

    public function down()
    {
        Schema::table('oxygen_quantities', function (Blueprint $table) {
            $table->renameColumn('ltr_5', '5_ltr');
            $table->renameColumn('ltr_10', '10_ltr');
            $table->renameColumn('ltr_15', '15_ltr');
        });
    }
}
