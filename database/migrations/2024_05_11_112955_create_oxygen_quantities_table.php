<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOxygenQuantitiesTable extends Migration
{
    public function up()
    {
        Schema::create('oxygen_quantities', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('5_ltr')->default(5);
            $table->integer('10_ltr')->default(5);
            $table->integer('15_ltr')->default(5);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('oxygen_quantities');
    }
}
