<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOxygenRequestsTable extends Migration
{
    public function up()
    {
        Schema::create('oxygen_requests', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('name');
            $table->string('gender');
            $table->unsignedInteger('age');
            $table->string('aadhar_card_number');
            $table->string('identity_proof'); // You may want to store the file path instead of the file itself
            $table->enum('covid_status', ['Positive', 'Negative']);
            $table->date('positive_date')->nullable();
            $table->text('address');
            $table->string('state');
            $table->string('city');
            $table->string('phone_number');
            $table->string('cylinder_options');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('oxygen_requests');
    }
}
