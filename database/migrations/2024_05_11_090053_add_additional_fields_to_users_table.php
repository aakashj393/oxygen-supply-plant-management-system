<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdditionalFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
          
            $table->enum('gender', ['male', 'female', 'other'])->nullable();

         
            $table->integer('age')->nullable();

            $table->bigInteger('aadhar_card_number')->nullable();

            $table->string('identity_proof')->nullable();

            $table->string('address')->nullable();

            $table->string('state')->nullable();

            $table->string('city')->nullable();

            $table->bigInteger('phone_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            // Drop columns if migration is rolled back
            $table->dropColumn(['gender', 'age', 'aadhar_card_number', 'identity_proof', 'address', 'state', 'city', 'phone_number']);
        });
    }
}
