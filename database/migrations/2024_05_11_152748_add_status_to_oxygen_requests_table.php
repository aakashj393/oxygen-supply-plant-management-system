<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusToOxygenRequestsTable extends Migration
{
    public function up()
    {
        Schema::table('oxygen_requests', function (Blueprint $table) {
            $table->tinyInteger('status')->default(0)->after('cylinder_options');
        });
    }

    public function down()
    {
        Schema::table('oxygen_requests', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
