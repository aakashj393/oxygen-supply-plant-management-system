<?php

namespace Database\Seeders;

use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\OxygenQuantity; 


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $user = User::create([
            'name' => 'Supplier  Super',
            'email' => 'supersupplier@example.com',
            'password' => bcrypt(12345678),
            'phone_number' => '1234567891',
            'gender' => 'Female',
            'age' => rand(18, 60),
            'aadhar_card_number' => '12345649109' ,
            'address' => 'Address ',
            'state' => 'Kerala',
            'city' => 'City ',
        ]);

        // Create oxygen quantities record for the user
        OxygenQuantity::create([
            'user_id' => $user->id,
            'ltr_5' => 5,
            'ltr_10' => 5,
            'ltr_15' => 5,
        ]);

        $this->call([
            UsersTableSeeder::class,
        ]);
    }
}
