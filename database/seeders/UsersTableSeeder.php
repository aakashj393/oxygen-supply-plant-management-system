<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\OxygenQuantity; 

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $states = [
            'Andhra Pradesh', 'Arunachal Pradesh', 'Assam', 'Bihar', 'Chhattisgarh',
            'Goa', 'Gujarat', 'Haryana', 'Himachal Pradesh', 'Jharkhand',
            'Karnataka', 'Kerala', 'Madhya Pradesh', 'Maharashtra', 'Manipur',
            'Meghalaya', 'Mizoram', 'Nagaland', 'Odisha', 'Punjab',
            'Rajasthan', 'Sikkim', 'Tamil Nadu', 'Telangana', 'Tripura',
            'Uttar Pradesh', 'Uttarakhand', 'West Bengal'
        ];
        for ($i = 1; $i <= 9; $i++) {
            // Create user 
            $random_number10 = mt_rand(1000000000, 9999999999);
            $pass = mt_rand(10000000, 99999999);
            $randomIndex=array_rand($states);
            $randomState = $states[$randomIndex];
            $user = User::create([
                'name' => 'Supplier ' . $i,
                'email' => 'supplier' . $i . '@example.com',
                'password' => bcrypt($pass),
                'phone_number' => $random_number10,
                'gender' => $i % 2 === 0 ? 'Male' : 'Female',
                'age' => rand(18, 60),
                'aadhar_card_number' => '12345678910' . $i,
                'address' => 'Address ' . $i,
                'state' => $randomState,
                'city' => 'City ' . $i,
            ]);

            // Create oxygen quantities record for the user
            OxygenQuantity::create([
                'user_id' => $user->id,
                'ltr_5' => 5,
                'ltr_10' => 5,
                'ltr_15' => 5,
            ]);
        }
    }
}
