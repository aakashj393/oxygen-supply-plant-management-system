@extends('layouts.app')

@section('content')
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light mb-3">
        <a class="navbar-brand" href="{{ route('home') }}">Oxygen Booking System</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('oxygen-requests.create') }}">Book Oxygen Cylinder</a>
                </li>
                @if(!(auth()->check()))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">Supplier Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">Supplier Register</a>
                    </li>
                @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('dashboard') }}">Supplier Panel</a>
                    </li>
                    <li class="nav-item">
                            <a class="nav-link" href="{{ route('logout') }}">Logout</a>
                    </li>
                @endif
              
            </ul>
        </div>
    </nav>
</div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Book Oxygen Cylinder') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('oxygen-requests.store') }}" enctype="multipart/form-data">
                        @csrf

                        <!-- Supplier Name (Dropdown) -->
                        <div class="form-group row">
                            <label for="user_id" class="col-md-4 col-form-label text-md-right">Supplier Name</label>
                            <div class="col-md-6">
                                <select class="form-control @error('user_id') is-invalid @enderror" id="user_id" name="user_id">
                                <option value="">Select Supplier</option>

                                    @foreach($users as $user)

                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                </select>
                                @error('user_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <!-- Name Text Field -->
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" >
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <!-- Gender Radio Buttons -->
                        <!-- <div class="form-group row">
                            <label>Gender</label><br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="gender" id="gender_male" value="Male" ]>
                                <label class="form-check-label" for="gender_male">Male</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="gender" id="gender_female" value="Female" >
                                <label class="form-check-label" for="gender_female">Female</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="gender" id="gender_female" value="Female" >
                                <label class="form-check-label" for="gender_female">Female</label>
                            </div>
                            @error('gender')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div> -->
                        <div class="form-group row ">
                            <label for="gender"class="col-md-4 col-form-label text-md-right" >{{ __('Gender') }}</label>
                            <div class="col-md-6">
                                
                                    <select id="gender" class="form-control @error('gender') is-invalid @enderror" name="gender" >
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                        <option value="other">Other</option>
                                    </select>

                                    @error('gender')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                               
                            </div>
                        </div>

                        <!-- Age Text Field -->
                        <div class="form-group row">
                            <label for="age" class="col-md-4 col-form-label text-md-right">Age</label>
                            <div class="col-md-6">
                                <input type="number" class="form-control @error('age') is-invalid @enderror" id="age" name="age" >
                                @error('age')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <!-- Aadhar Card Number Text Field -->
                        <div class="form-group row">
                            <label for="aadhar_card_number" class="col-md-4 col-form-label text-md-right">Aadhar Card Number</label>
                            <div class="col-md-6">
                                <input type="number" class="form-control @error('aadhar_card_number') is-invalid @enderror" id="aadhar_card_number" name="aadhar_card_number" >
                                @error('aadhar_card_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <!-- Identity Proof File Input -->
                        <div class="form-group row">
                            <label for="identity_proof" class="col-md-4 col-form-label text-md-right">Identity Proof</label>
                            <div class="col-md-6">
                                <input type="file" class="form-control-file @error('identity_proof') is-invalid @enderror" id="identity_proof" name="identity_proof" >
                                @error('identity_proof')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <!-- Covid-19 Dropdown -->
                        <div class="form-group row">
                            <label for="covid_status" class="col-md-4 col-form-label text-md-right">Covid-19</label>
                            <div class="col-md-6">
                                <select class="form-control @error('covid_status') is-invalid @enderror" id="covid_status" name="covid_status" >
                                    <option value="">Select Status</option>
                                    <option value="Positive">Positive</option>
                                    <option value="Negative">Negative</option>
                                </select>
                                @error('covid_status')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <!-- Date of Covid-19 Positive Date Picker -->
                        <div class="form-group row" id="positive_date_input" >
                            <label for="positive_date" class="col-md-4 col-form-label text-md-right">Date of Covid-19 positive</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control @error('positive_date') is-invalid @enderror" id="positive_date" name="positive_date">
                                @error('positive_date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <!-- Address Text Field -->
                        <div class="form-group row">
                            <label for="address" class="col-md-4 col-form-label text-md-right">Address</label>
                            <div class="col-md-6">
                                <textarea class="form-control @error('address') is-invalid @enderror" id="address" name="address" rows="3" ></textarea>
                                @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <!-- State Dropdown -->
                        <div class="form-group row">
                            <label for="state" class="col-md-4 col-form-label text-md-right">State</label>
                            <div class="col-md-6">
                                <select class="form-control @error('state') is-invalid @enderror" id="state" name="state" >
                                    <option value="">Select State</option>
                                    @foreach($states as $state)
                                        <option value="{{ $state }}">{{ $state }}</option>
                                    @endforeach
                                </select>
                                @error('state')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <!-- City Dropdown (dependent based on state value) -->
                        <div class="form-group row">
                            <label for="city" class="col-md-4 col-form-label text-md-right">City</label>
                            <div class="col-md-6">
                                <input id="city" type="text" class="form-control @error('city') is-invalid @enderror" name="city" value="{{ old('city') }}" >

                                @error('city')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <!-- Phone Number Text Field -->
                        <div class="form-group row">
                            <label for="phone_number" class="col-md-4 col-form-label text-md-right">Phone Number</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control @error('phone_number') is-invalid @enderror" id="phone_number" name="phone_number" >
                                @error('phone_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <!-- Cylinder Options Dropdown -->
                        <div class="form-group row">
                            <label for="cylinder_options" class="col-md-4 col-form-label text-md-right">Cylinder Options</label>
                            <div class="col-md-6">
                                <select class="form-control @error('cylinder_options') is-invalid @enderror" id="cylinder_options" name="cylinder_options" >
                                    <option value="">Select Cylinder Size</option>
                                    <option value="ltr_5">5 Ltr</option>
                                    <option value="ltr_10">10 Ltr</option>
                                    <option value="ltr_15">15 Ltr</option>
                                </select>
                                @error('cylinder_options')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#positive_date').datepicker({
            dateFormat: 'yy-mm-dd', // Format of the date
            changeMonth: true, // Allow changing of months
            changeYear: true, // Allow changing of years
            yearRange: '-100:+0', // Set range for years
        });
        $('#positive_date_input').hide();
        $('#covid_status').change(function() {
            if ($(this).val() === 'Positive') {
                $('#positive_date_input').show();
            } else {
                $('#positive_date_input').hide();
            }
        });


            $('#user_id').on('change', function () {
                var user_id = $(this).val();
                $.ajax({
                    url: "{{ route('getQty') }}",
                    type: "GET",
                    data: {
                        'user_id': user_id
                    },
                    success: function (data) {
                        ltr_5= data.oxygenQuantity.ltr_5;
                        ltr_10= data.oxygenQuantity.ltr_10;
                        ltr_15= data.oxygenQuantity.ltr_15;
                        if (ltr_5  <= 0) {
                            $('#cylinder_options option[value="ltr_5"]').hide();

                        } 
                        if (ltr_10  <= 0) {
                            $('#cylinder_options option[value="ltr_10"]').hide();

                        }
                        if (ltr_15   <= 0) {
                            $('#cylinder_options option[value="ltr_15"]').hide();

                        }
                    }
                });

            });
    });
</script>

@endsection



