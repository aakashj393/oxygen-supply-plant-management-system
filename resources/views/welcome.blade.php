
@extends('layouts.app')

@section('content')
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light mb-3">
        <a class="navbar-brand" href="{{ route('home') }}">Oxygen Booking System</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('oxygen-requests.create') }}">Book Oxygen Cylinder</a>
                </li>
                @if(!(auth()->check()))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">Supplier Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">Supplier Register</a>
                    </li>
                @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('dashboard') }}">Supplier Panel</a>
                    </li>
                    <li class="nav-item">
                            <a class="nav-link" href="{{ route('logout') }}">Logout</a>
                    </li>
                @endif
              
            </ul>
        </div>
    </nav>
</div>
<div class="container supplier"  >
    <div class="mb-3">
        <button type="submit" class="btn btn-primary" name="filter"  id="state" value="state">Filter by State</button>
        <button type="submit" class="btn btn-primary" name="filter"  id="supplier" value="supplier">Filter by Supplier</button>
    </div>
    <div class="table-responsive" id="supplierTable">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Sr No</th>
                    <th>Supplier Name</th>
                    <th>Supplier State</th>
                    <th>5 Ltr</th>
                    <th>10 Ltr</th>
                    <th>15 Ltr</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $counter = 1 + ($oxygenQuantities->currentPage() - 1) * $oxygenQuantities->perPage();
                @endphp
                @foreach($oxygenQuantities as $quantity)
                <tr>
                    <td>{{ $counter++ }}</td>
                    <td>{{ $quantity->user->name }}</td>
                    <td>{{ $quantity->user->state }}</td>
                    <td>{{ $quantity->ltr_5 }}</td>
                    <td>{{ $quantity->ltr_10 }}</td>
                    <td>{{ $quantity->ltr_15 }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {!! $oxygenQuantities->links() !!}
    </div>
</div>

<div class="container" id="stateTable" style="display: none;">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Sr.No</th>
                <th>State</th>
                <th>5 Litre</th>
                <th>10 Litre</th>
                <th>15 Litre</th>
            </tr>
        </thead>
        <tbody>
                @php
                    $counter2 = 1 + ($stateWiseQuantities->currentPage() - 1) * $stateWiseQuantities->perPage();
                @endphp
            @foreach ($stateWiseQuantities as $stateQuantity)
            <tr>
                <td>{{ $counter2++ }}</td>   
                <td>{{ $stateQuantity->state }}</td>
                <td>{{ $stateQuantity->total_ltr_5 }}</td>
                <td>{{ $stateQuantity->total_ltr_10 }}</td>
                <td>{{ $stateQuantity->total_ltr_15 }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {!! $stateWiseQuantities->links() !!}

</div>
<script>
    $(document).ready(function() {
        $('#state').click(function() {
            $('#stateTable').show();
            $('#supplierTable').hide();
        });

        $('#supplier').click(function() {
            $('#stateTable').hide();
            $('#supplierTable').show();
        });
    });
</script>

@endsection
