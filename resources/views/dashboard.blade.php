
@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <a class="navbar-brand" href="{{ route('home') }}">Oxygen Booking System</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('home') }}">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('logout') }}">Logout</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>

        <h1 class="mt-4">Supplier Panel</h1>

        <!-- <form method="get" action="{{ route('logout') }}">
            @csrf
            <button type="submit" class="btn btn-primary">Logout</button>
        </form> -->
        <div class="container">
            <!-- <h4 class="mt-4">Search Oxygen Quantities</h4> -->
            <form method="get" action="{{ route('filter') }}">
                <div class="row mt-3">
                    <div class="col-md-4">
                        <!-- <label for="state" class="form-label">Select State:</label> -->
                        <select class="form-control" id="state" name="state">
                            <option value="">Select State</option>
                            @foreach($states as $state)
                                <option value="{{ $state }}">{{ $state }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4">
                        <!-- <label for="cylinder_size" class="form-label">Select Cylinder Size:</label> -->
                        <select class="form-control" id="cylinder_size" name="cylinder_size">
                            <option value="">Cylinder Options</option>
                            <option value="ltr_5">5 Litre</option>
                            <option value="ltr_10">10 Litre</option>
                            <option value="ltr_15">15 Litre</option>
                        </select>
                    </div>
                    <div class="col-md-4 d-flex align-items-end " >
                        <button type="submit" class=" btn btn-primary mt-2">Search</button>
                    </div>
                </div>
            </form>
        </div>
        @if($oxygenRequest->total() > 0)
     

            <div class="table-responsive">
     

                <table class="table table-striped">
                    <!-- Display your table headers -->
                    <thead class="thead-dark">
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Gender</th>
                            <th>Age</th>
                            <th>Aadhar Card Number</th>
                            <th>Identity Proof</th>
                            <th>Covid Status</th>
                            <th>Positive Date</th>
                            <th>Address</th>
                            <th>State</th>
                            <th>City</th>
                            <th>Phone Number</th>
                            <th>Cylinder Option</th>
                            <th>Status</th>  
                            <th>Change Status</th>  
                        </tr>
                    </thead>
                    <tbody>
                    @php
                        $counter = 1 + ($oxygenRequest->currentPage() - 1) * $oxygenRequest->perPage();
                    @endphp
                        @foreach($oxygenRequest as $request)
                            <tr>
                                <td>{{ $counter++ }}</td>
                                <td>{{ $request->name }}</td>
                                <td>{{ $request->gender }}</td>
                                <td>{{ $request->age }}</td>
                                <td>{{ $request->aadhar_card_number }}</td>
                                <td><img src="{{ asset( $request->identity_proof ) }}" style="width: 100px; height: auto;" alt="Identity Proof"></td>
                                <td>{{ $request->covid_status }}</td>
                                <td>{{ $request->positive_date }}</td>
                                <td>{{ $request->address }}</td>
                                <td>{{ $request->state }}</td>
                                <td>{{ $request->city }}</td>
                                <td>{{ $request->phone_number }}</td>
                                <td>  @if($request->cylinder_options === 'ltr_5')
                                        5 Litre
                                    @elseif($request->cylinder_options === 'ltr_10')
                                        10 Litre
                                    @elseif($request->cylinder_options === 'ltr_15')
                                        15 Litre
                                    @else
                                        Unknown Size
                                    @endif
                                </td>
                                <td>{{ $request->status == 0 ? 'Processing' : ($request->status == 1 ? 'Delivered' : 'Cancelled') }}</td>
                                <td>
                                    <a href="#" class="edit-link" data-toggle="modal" data-request-id="{{ $request->id }}" data-target="#editModal">Edit</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {!! $oxygenRequest->links() !!}
        @else
            <p>No records found.</p>
        @endif

    </div>
   
    <!-- <div class="d-flex justify-content-center">
        {{ $oxygenRequest->links() }}
    </div> -->

</div>

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">Update Request</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Form inside the modal -->
                <form id="editForm" action="{{ route('editStatus') }}" method="POST">
                    @csrf

                    <div class="form-group">
                    <div class="form-group">
                    <input type="hidden" name="request_id" id="hiddenId" value="">
                    <label for="status">Select Status:</label>
                    <select class="form-control" name="status" id="status">
                        <option value="0">Processing</option>
                        <option value="1">Delivered</option>
                        <option value="2">Cancelled</option>
                    </select>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id="submitForm">Submit</button>
                    </div>
                </form>
            </div>
            
        </div>
    </div>
</div>


    <script>
        $(document).ready(function() {
        // Click event handler for the "Edit" link
        $('.edit-link').click(function(event) {
            event.preventDefault(); // Prevent default link behavior

            // Get the request ID from data attribute or any other source
            var requestId = $(this).data('request-id');
            
            // Set the request ID value to the hidden input field
            $('#hiddenId').val(requestId);

            // Show the modal
            $('#editModal').modal('show');
        });

    });
    </script>

@endsection
