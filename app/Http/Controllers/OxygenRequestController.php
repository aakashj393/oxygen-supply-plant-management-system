<?php
namespace App\Http\Controllers;
use App\Models\User;
use App\Models\OxygenRequest;
use App\Models\OxygenQuantity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use Illuminate\Support\Facades\Auth;

class OxygenRequestController extends Controller
{

    public function create()
    {
        $users = User::all();
        $states = [
            'Andhra Pradesh', 'Arunachal Pradesh', 'Assam', 'Bihar', 'Chhattisgarh',
            'Goa', 'Gujarat', 'Haryana', 'Himachal Pradesh', 'Jharkhand',
            'Karnataka', 'Kerala', 'Madhya Pradesh', 'Maharashtra', 'Manipur',
            'Meghalaya', 'Mizoram', 'Nagaland', 'Odisha', 'Punjab',
            'Rajasthan', 'Sikkim', 'Tamil Nadu', 'Telangana', 'Tripura',
            'Uttar Pradesh', 'Uttarakhand', 'West Bengal'
        ];
        return view('oxygen_request_form', [
            'users' => $users,
            'states' => $states,
        ]);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'user_id' => 'required|exists:users,id',
            'name' => 'required|string',
            'gender' => 'required|in:male,female,other',
            'age' => 'required|integer|min:18',
            'aadhar_card_number' => 'required|string|min:12',
            'identity_proof' => 'required|file',
            'covid_status' => 'required|in:Positive,Negative',
            'positive_date' => ($request->covid_status == 'Positive') ? 'required|date' : '', // Conditional validation
            'address' => 'required|string',
            'state' => 'required|string',
            'city' => 'required|string',
            'phone_number' => 'required|string',
            'cylinder_options' => 'required|in:ltr_5,ltr_10,ltr_15',
        ]);

        // if ($request->hasFile('identity_proof')) {
        //     $file = $request->file('identity_proof');
        //     $path = $file->store('identity_proofs'); // Store the file and get the path
        //     $validatedData['identity_proof'] = $path; // Update the path in the validated data
        // }

        if ($request->hasFile('identity_proof')) {
            $file = $request->file('identity_proof');
            $fileName = time() . '_' . $file->getClientOriginalName();
            $file->move(public_path('identity_proof'), $fileName);
            $validatedData['identity_proof'] = "identity_proof/".$fileName;
        }
        $OxygenRequest=OxygenRequest::create($validatedData);

        $oxygenQuantity = OxygenQuantity::where('user_id', $request->user_id)->firstOrFail();
      

        $cylinderSize = $OxygenRequest->cylinder_options;
       
        
        $newAvailableQuantity = bcsub($oxygenQuantity->$cylinderSize, '1');

        OxygenQuantity::where('user_id', $request->user_id)->update([
            $cylinderSize => $newAvailableQuantity
        ]);

        return redirect(url('/'));
    }
    public function getQty(Request $request){
        $oxygenQuantity = OxygenQuantity::where('user_id', $request->user_id)->firstOrFail();
        return response()->json([
            'oxygenQuantity' => $oxygenQuantity
        ]);
    }

    public function editStatus(Request $request){

        // Log::info($request->status);
        // Log::info($request->request_id);

        $validatedData = $request->validate([
            'status' => 'required|in:0,1,2',
        ]);
        OxygenRequest::where('id', $request->request_id)->update([
            'status' => $request->status
        ]);
        $supplier = Auth::user();
        $supplierId=$supplier->id;
        $oxygenRequest=OxygenRequest::where('user_id', $supplierId)->latest()->paginate(10);

        return redirect()->route('dashboard')->with('oxygenRequest', $oxygenRequest);

    }
}
