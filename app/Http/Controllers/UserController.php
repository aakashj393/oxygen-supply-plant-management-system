<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Log;
use App\Models\OxygenQuantity; 

class UserController extends Controller
{

  

    public function showRegistrationForm()
    {
        $states = [
            'Andhra Pradesh', 'Arunachal Pradesh', 'Assam', 'Bihar', 'Chhattisgarh',
            'Goa', 'Gujarat', 'Haryana', 'Himachal Pradesh', 'Jharkhand',
            'Karnataka', 'Kerala', 'Madhya Pradesh', 'Maharashtra', 'Manipur',
            'Meghalaya', 'Mizoram', 'Nagaland', 'Odisha', 'Punjab',
            'Rajasthan', 'Sikkim', 'Tamil Nadu', 'Telangana', 'Tripura',
            'Uttar Pradesh', 'Uttarakhand', 'West Bengal'
        ];
        return view('register', [
            'states' => $states,
        ]);
    }

    public function register(Request $request)
    {
        // Log::info('Logger');

        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'gender' => 'required|in:male,female,other',
            'age' => 'required|integer|min:0',
            'aadhar_card_number' => 'required|string|min:12|max:12|unique:users',
            'identity_proof' => 'nullable|file',
            'address' => 'required|string|max:255',
            'state' => 'required|string|max:255',
            'city' => 'required|string|max:255',
            'phone_number' => 'required|string|min:10|max:10|unique:users',
        ]);

        if ($request->hasFile('identity_proof')) {
            $file = $request->file('identity_proof');
            $fileName = time() . '_' . $file->getClientOriginalName();
            $file->move(public_path('uploads'), $fileName);
        }

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->gender = $request->gender;
        $user->age = $request->age;
        $user->aadhar_card_number = $request->aadhar_card_number;
        $user->identity_proof = "uploads/".$fileName ?? null;
        $user->address = $request->address;
        $user->state = $request->state;
        $user->city = $request->city;
        $user->phone_number = $request->phone_number;
        
        if ($user->save()) {
            OxygenQuantity::create([
                'user_id' => $user->id,
                'ltr_5' => 5,
                'ltr_10' => 5,
                'ltr_15' => 5,
            ]);
        }
      
        return redirect('/login')->with('success', 'Registration successful! Please login.');
    }
}
