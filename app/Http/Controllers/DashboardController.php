<?php
namespace App\Http\Controllers;
use App\Models\User;
use App\Models\OxygenRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Models\OxygenQuantity;


class DashboardController extends Controller
{
    /**
     * Show the dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $states = [
            'Andhra Pradesh', 'Arunachal Pradesh', 'Assam', 'Bihar', 'Chhattisgarh',
            'Goa', 'Gujarat', 'Haryana', 'Himachal Pradesh', 'Jharkhand',
            'Karnataka', 'Kerala', 'Madhya Pradesh', 'Maharashtra', 'Manipur',
            'Meghalaya', 'Mizoram', 'Nagaland', 'Odisha', 'Punjab',
            'Rajasthan', 'Sikkim', 'Tamil Nadu', 'Telangana', 'Tripura',
            'Uttar Pradesh', 'Uttarakhand', 'West Bengal'
        ];
        $supplier = Auth::user();
        $supplierId=$supplier->id;

        if ($request->state && $request->cylinder_size) {
             $oxygenRequest=OxygenRequest::where('user_id', $supplierId)
             ->where('state', $request->state)
             ->where('cylinder_options', $request->cylinder_size)
             ->latest()->paginate(10);
        } elseif ($request->state) {
            $oxygenRequest=OxygenRequest::where('user_id', $supplierId)
            ->where('state', $request->state)
            ->latest()->paginate(10);
        }elseif ($request->cylinder_size) {
            $oxygenRequest=OxygenRequest::where('user_id', $supplierId)
            ->where('cylinder_options', $request->cylinder_size)
            ->latest()->paginate(10);
        }else{
            $oxygenRequest=OxygenRequest::where('user_id', $supplierId)->latest()->paginate(10);
        }
       
     

   
        // Log::info($oxygenRequest);
        return view('dashboard', [
            'oxygenRequest' => $oxygenRequest,
            'states' =>$states
        ]);
        // return view('dashboard');
    }

    

    public function home()
    {
        $stateWiseQuantities = User::with('oxygenQuantities')
        ->select('state')
        ->selectRaw('SUM(ltr_5) as total_ltr_5, SUM(ltr_10) as total_ltr_10, SUM(ltr_15) as total_ltr_15')
        ->leftJoin('oxygen_quantities', 'users.id', '=', 'oxygen_quantities.user_id')
        ->groupBy('state')
        ->paginate();
        $oxygenQuantities = OxygenQuantity::with('user')->paginate(10);
        return view('welcome', [
            'oxygenQuantities' => $oxygenQuantities,
            'stateWiseQuantities' => $stateWiseQuantities,
        ]);
    }
}
