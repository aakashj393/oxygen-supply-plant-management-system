<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OxygenRequest extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'name',
        'gender',
        'age',
        'aadhar_card_number',
        'identity_proof',
        'covid_status',
        'positive_date',
        'address',
        'state',
        'city',
        'phone_number',
        'cylinder_options',
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    } 
}
