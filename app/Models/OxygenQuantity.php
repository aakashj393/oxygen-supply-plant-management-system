<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OxygenQuantity extends Model
{
    use HasFactory;

    protected $table = 'oxygen_quantities';

    protected $fillable = [
        'user_id',
        'ltr_5',
        'ltr_10',
        'ltr_15',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
}
